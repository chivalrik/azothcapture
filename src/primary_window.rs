use std::fmt::{Display, Formatter};
use std::rc::Rc;

use druid::widget::prelude::*;
use druid::widget::{
    Button, Controller, CrossAxisAlignment, Either, Flex, Label, List, ListIter, Painter,
    RadioGroup, Scroll, Spinner, TextBox, ValueTextBox,
};
use druid::{
    AppLauncher, Color, Data, HasRawWindowHandle, Lens, RawWindowHandle, Selector, UnitPoint,
    WidgetExt, WindowDesc, WindowLevel,
};
use im::Vector;
use serde::{Deserialize, Serialize};

use crate::model::{AppState, DialogueRect, HotKey, TranslationEntry};
use std::collections::HashMap;

pub(crate) const SETTINGS_PATH_TOML: &str = "azothcapture.toml";
pub(crate) const SETTINGS_PATH_RON: &str = "azothcapture.ron";
pub(crate) const CSV_EXPORT_PATH: &str = "captured_lines.csv";
pub(crate) const HOTKEY_PRESSED: Selector = Selector::new("azothcapture.hotkey_pressed");
pub(crate) const CAPTURE_CLOSED: Selector = Selector::new("azothcapture.abort_capturing");
pub(crate) const CAPTURE_RECT: Selector<druid::Rect> = Selector::new("azothcapure.capture_rect");
pub(crate) const CAPTURE_WINDOW: Selector = Selector::new("azothcapure.capture_window");
pub(crate) const REDO_OCR: Selector<TranslationEntry> = Selector::new("azothcapure.redo_ocr");
pub(crate) const ADD_TRANSLATION_ENTRY: Selector<TranslationEntry> =
    Selector::new("azothcapure.add_translation_entry");
pub(crate) const VERTICAL_WIDGET_SPACING: f64 = 20.0;
pub(crate) const TEXT_BOX_WIDTH: f64 = 200.0;
pub(crate) const HEADER_BACKGROUND: Color = Color::grey8(0xCC);
pub(crate) const REGULAR_TEXT: Color = Color::rgb8(0xf0, 0xf0, 0xea);
pub(crate) const TEXT_COLOR: druid::Color = druid::Color::grey8(0x11);
pub(crate) const TABLE_HEADER: &[(&str, f64)] = &[
    ("Line", 50.0),
    ("OCR", 50.0),
    //("Speaker", 80.0),
    //("English", 250.0),
    ("Japanese", 270.0),
    ("Speaker", 100.0),
    ("Pic", 50.0),
    ("Pic Sp.", 50.0),
    ("Chapter", 80.0),
    ("Meta", 250.0),
    ("Box", 200.0),
];

#[derive(Eq, PartialEq, Hash)]
pub(crate) enum TranslationEntries {
    Line,
}
// NOTE ah, blurgh
static TRANSLATION_ENTRIES: once_cell::sync::Lazy<HashMap<TranslationEntries, (&str, f64)>> =
    once_cell::sync::Lazy::new(|| HashMap::from([(TranslationEntries::Line, ("Line", 50.))]));

pub(crate) fn create() -> (WindowDesc<AppState>, Delegate) {
    // describe the main window
    let primary_window = WindowDesc::new(build_root_widget())
        .title("AzothCapture")
        .window_size((1400.0, 600.0))
        .with_min_size((400.0, 200.0));

    (primary_window, Delegate {})
}

pub(crate) struct Delegate {}
impl druid::AppDelegate<AppState> for Delegate {
    fn command(
        &mut self,
        _ctx: &mut druid::DelegateCtx,
        _target: druid::Target,
        _cmd: &druid::Command,
        _data: &mut AppState,
        _env: &Env,
    ) -> druid::Handled {
        druid::Handled::No
    }

    fn window_added(
        &mut self,
        id: druid::WindowId,
        data: &mut AppState,
        env: &Env,
        ctx: &mut druid::DelegateCtx,
    ) {
    }
}

pub struct IconController {}
impl<T, W: druid::Widget<T>> Controller<T, W> for IconController {
    fn event(&mut self, child: &mut W, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        match event {
            Event::WindowConnected => {
                let handle = ctx.window().raw_window_handle();
                match handle {
                    RawWindowHandle::Windows(handle) => unsafe {
                        use winapi::um::winuser::*;
                        let icon = LoadIconW(handle.hinstance as _, MAKEINTRESOURCEW(1));
                        log::debug!("0x{:x}", icon as usize);
                        if !icon.is_null() {
                            SendMessageW(
                                handle.hwnd as _,
                                winapi::um::winuser::WM_SETICON,
                                winapi::um::winuser::ICON_SMALL as _,
                                icon as _,
                            );
                            SendMessageW(
                                handle.hwnd as _,
                                winapi::um::winuser::WM_SETICON,
                                winapi::um::winuser::ICON_BIG as _,
                                icon as _,
                            );
                        } else {
                            log::warn!("Icon Handle was null.");
                        }
                    },
                    _ => {}
                };
            }
            _ => {}
        }
        child.event(ctx, event, data, env)
    }
}

struct PrimaryController<'a> {
    time_format: Vec<time::format_description::FormatItem<'a>>,
}

impl PrimaryController<'_> {
    fn show_capture_window(
        &mut self,
        ctx: &mut EventCtx,
        data: &mut AppState,
        env: &Env,
        pixels_raw: Vec<u8>,
        width: i32,
        height: i32,
        maximize: bool,
    ) {
        let time_now = time::OffsetDateTime::now_utc();
        // NOTE (Chiv) That should not fail as we provided a correct format
        let time_now = time_now.format(&self.time_format).unwrap();
        let region_capture = crate::crop_region_window::capture_root(
            pixels_raw,
            width as usize,
            height as usize,
            time_now,
        );
        let window_config = druid::WindowConfig::default()
            .show_titlebar(false)
            .set_level(WindowLevel::Modal(ctx.window().clone()))
            .resizable(false);
        let window_config = if maximize {
            window_config.set_window_state(druid::WindowState::Maximized)
        } else {
            let screen = druid::Screen::get_monitors()
                .into_iter()
                .find(|m| m.is_primary())
                .map(|m| m.virtual_rect())
                .unwrap();
            let screen_width = screen.x1 - screen.x0;
            let screen_height = screen.y1 - screen.y0;
            window_config
                .set_position(druid::Point::new(
                    (screen_width / 2.0 - width as f64 / 2.0),
                    (screen_height / 2.0 - height as f64 / 2.0),
                ))
                .window_size((width as f64, height as f64))
        };
        ctx.new_sub_window(
            window_config,
            region_capture,
            crate::crop_region_window::CropRegionState {
                is_drawing_rect: false,
                rect_top_left_line: (0, 0),
                rect_bottom_right_line: (0, 0),
                rect_top_left_speaker: (0, 0),
                rect_bottom_right_speaker: (0, 0),
                current_chapter: data.translation.current_chapter.clone(),
                next_line: data.current_line,
                target: Default::default(),
                sender: data.sender.clone(),
            },
            env.clone(),
        );
        data.current_line += 1;
    }
}

impl<W: druid::Widget<AppState>> Controller<AppState, W> for PrimaryController<'_> {
    fn event(
        &mut self,
        child: &mut W,
        ctx: &mut EventCtx,
        event: &Event,
        data: &mut AppState,
        env: &Env,
    ) {
        match event {
            Event::WindowConnected => {
                data.visible_windows = std::sync::Arc::new(crate::ffi::enumerate_windows());
            }
            Event::WindowCloseRequested => {}
            Event::WindowDisconnected => {}
            Event::WindowSize(_) => {}
            Event::MouseDown(_) => {}
            Event::MouseUp(_) => {}
            Event::MouseMove(_) => {}
            Event::Wheel(_) => {}
            Event::KeyDown(_) => {}
            Event::KeyUp(_) => {}
            Event::Paste(_) => {}
            Event::Zoom(_) => {}
            Event::Timer(_) => {}
            Event::AnimFrame(_) => {}
            Event::Command(c) => {
                if let Some(r) = c.get(CAPTURE_RECT) {
                    if !data.is_capturing_disabled {
                        let width = r.x1 - r.x0;
                        let height = r.y1 - r.y0;
                        let (pixels_raw, width, height) = unsafe {
                            crate::ffi::capture_desktop_raw(
                                r.x0 as i32,
                                r.y0 as i32,
                                width as i32,
                                height as i32,
                            )
                        };
                        data.is_capturing_disabled = true;
                        self.show_capture_window(ctx, data, env, pixels_raw, width, height, true);
                    }
                }
                if let Some(t) = c.get(ADD_TRANSLATION_ENTRY) {
                    if let Some(e) = data.translation.list.iter_mut().find(|e| e.line == t.line) {
                        // Save old translation
                        // TODO OldOCR -> display
                        e.metadata = if e.metadata.is_empty() {
                            format!(
                                "{}:{}\n{}",
                                e.ocr_engine, e.speaker_japanese, e.line_japanese
                            )
                        } else {
                            format!(
                                "{}\n{}:{}\n{}",
                                e.metadata, e.ocr_engine, e.speaker_japanese, e.line_japanese
                            )
                        };
                        e.speaker_japanese = t.speaker_japanese.clone();
                        e.line_japanese = t.line_japanese.clone();
                        e.ocr_engine = t.ocr_engine;
                        e.processing_ocr = false;
                    } else {
                        data.translation.list.push_front(TranslationEntry {
                            processing_ocr: false,
                            ..t.clone()
                        });
                    }
                }
                if let Some(e) = c.get(REDO_OCR) {
                    data.sender.send(e.clone());
                }
                if c.is(CAPTURE_CLOSED) {
                    data.is_capturing_disabled = false;
                }
                if c.is(CAPTURE_WINDOW) {
                    if !data.is_capturing_disabled
                        && !data.translation.current_capture_window.is_empty()
                    {
                        //TODO Seems we need to fresh our handle? Maybe just search for title by name?
                        data.visible_windows = std::sync::Arc::new(crate::ffi::enumerate_windows());
                        if let Some((hwnd, _)) = data.visible_windows.iter().find(|(hwnd, name)| {
                            name.starts_with(&data.translation.current_capture_window)
                        }) {
                            let hwnd = *hwnd;
                            let (pixels_raw, width, height) =
                                unsafe { crate::ffi::capture_window_raw_via_desktop(hwnd as _) };
                            data.is_capturing_disabled = true;
                            self.show_capture_window(
                                ctx, data, env, pixels_raw, width, height, false,
                            );
                        }
                    }
                }
                if c.is(HOTKEY_PRESSED) {
                    match data.translation.hot_key {
                        HotKey::Window => ctx.submit_command(CAPTURE_WINDOW),
                        HotKey::Primary => {
                            let rect = druid::Screen::get_monitors()
                                .into_iter()
                                .find(|m| m.is_primary())
                                .unwrap()
                                .virtual_rect();
                            ctx.submit_command(CAPTURE_RECT.with(rect))
                        }
                    }
                }
            }
            Event::Notification(_) => {}
            Event::ImeStateChange => {}
            Event::Internal(_) => {}
            _ => {}
        }
        child.event(ctx, event, data, env)
    }

    fn lifecycle(
        &mut self,
        child: &mut W,
        ctx: &mut LifeCycleCtx,
        event: &LifeCycle,
        data: &AppState,
        env: &Env,
    ) {
        child.lifecycle(ctx, event, data, env)
    }

    fn update(
        &mut self,
        child: &mut W,
        ctx: &mut UpdateCtx,
        old_data: &AppState,
        data: &AppState,
        env: &Env,
    ) {
        child.update(ctx, old_data, data, env);
        if old_data.translation != data.translation {
            data.translation.save_toml(SETTINGS_PATH_TOML.as_ref());
        }
    }
}

fn build_root_widget() -> impl Widget<AppState> {
    Flex::column()
        .with_child(settings_area())
        .with_default_spacer()
        .with_flex_child(translation_list(), 1.0)
        // NOTE  That parse string is correct
        .controller(PrimaryController {
            time_format: time::format_description::parse(
                "[year]-[month]-[day]T[hour].[minute].[second]",
            )
            .unwrap(),
        })
        .controller(IconController {})
}
fn translation_list() -> impl Widget<AppState> {
    let mut header = Flex::row()
        .must_fill_main_axis(true)
        //.with_default_spacer()
        .with_child(
            Label::new(TABLE_HEADER[0].0)
                .with_text_color(TEXT_COLOR)
                //.with_text_alignment(druid::TextAlignment::Center)
                //.expand_width()
                .fix_width(TABLE_HEADER[0].1)
                .background(HEADER_BACKGROUND),
            //.padding(10.0),
            //TABLE_HEADER[0].1,
        );
    for (name, size) in TABLE_HEADER.iter().skip(1) {
        // Keep in mind that later on, in the main function,
        // we set the default spacer values. Without explicitly
        // setting them the default spacer is bigger, and is
        // probably not desirable for your purposes.
        header.add_default_spacer();
        header.add_child(
            Label::new(*name)
                .with_text_color(TEXT_COLOR)
                //.with_text_alignment(druid::TextAlignment::Center)
                //.expand_width()
                .fix_width(*size)
                .background(HEADER_BACKGROUND),
            //.padding(10.0),
            //*size,
        );
    }
    Scroll::new(
        Flex::column()
            .cross_axis_alignment(CrossAxisAlignment::Start)
            .with_child(header)
            .with_default_spacer()
            .with_flex_child(
                Scroll::new(
                    Scroll::new(
                        List::new(make_list_item).lens(druid::lens!(AppState, translation.list)),
                    )
                    .vertical(),
                ),
                1.0,
            )
            .background(Color::WHITE),
    )
    .horizontal()
    .padding(10.0)
}

fn make_list_item() -> impl Widget<TranslationEntry> {
    Flex::row()
        .with_child(
            Scroll::new(
                Label::dynamic(|d: &TranslationEntry, _| format!("{:06}", d.line))
                    .with_text_color(TEXT_COLOR)
                    .fix_width(TABLE_HEADER[0].1),
            )
            .horizontal(),
        )
        .with_default_spacer()
        // .with_child(
        //     Label::dynamic(|d: &TranslationEntry, _| d.speaker_english.clone())
        //         .fix_width(TABLE_HEADER[1].1),
        // )
        // .with_default_spacer()
        // .with_child(
        //     Label::dynamic(|d: &TranslationEntry, _| d.line_english.clone())
        //         .fix_width(TABLE_HEADER[2].1),
        // )
        .with_default_spacer()
        .with_child(Either::new(
            |d: &TranslationEntry, _env| d.processing_ocr,
            Spinner::new().with_color(TEXT_COLOR),
            Button::from_label(Label::new("OCR"))
                .fix_width(TABLE_HEADER[1].1)
                .on_click(move |ctx, data: &mut TranslationEntry, _env| {
                    data.processing_ocr = true;
                    let e = crate::model::TranslationEntry {
                        ocr_engine: data.ocr_engine.next(),
                        ..data.clone()
                    };
                    ctx.submit_command(REDO_OCR.with(e));
                }),
        ))
        .with_default_spacer()
        .with_child(
            Scroll::new(
                TextBox::multiline()
                    .fix_width(TABLE_HEADER[2].1)
                    .lens(TranslationEntry::line_japanese),
            )
            .horizontal(),
        )
        .with_default_spacer()
        .with_child(
            Scroll::new(
                TextBox::new()
                    .fix_width(TABLE_HEADER[3].1)
                    .lens(TranslationEntry::speaker_japanese),
            )
            .horizontal(),
        )
        .with_default_spacer()
        .with_child(
            Scroll::new(
                // TODO image on hover
                Label::dynamic(|d: &TranslationEntry, _| d.file_name_line[9..].to_string())
                    .with_text_color(TEXT_COLOR)
                    .fix_width(TABLE_HEADER[4].1),
            )
            .horizontal(),
        )
        .with_default_spacer()
        .with_child(
            Scroll::new(
                Label::dynamic(|d: &TranslationEntry, _| {
                    d.file_name_speaker
                        .as_ref()
                        .map(|n| {
                            if n.len() > 8 {
                                n[9..].to_string()
                            } else {
                                n.clone()
                            }
                        })
                        .unwrap_or_else(|| "".into())
                })
                .with_text_color(TEXT_COLOR)
                .fix_width(TABLE_HEADER[5].1),
            )
            .horizontal(),
        )
        .with_default_spacer()
        .with_child(
            Scroll::new(
                TextBox::new()
                    .fix_width(TABLE_HEADER[6].1)
                    .lens(TranslationEntry::chapter),
            )
            .horizontal(),
        )
        .with_default_spacer()
        .with_child(
            Scroll::new(
                TextBox::multiline()
                    .fix_width(TABLE_HEADER[7].1)
                    .lens(TranslationEntry::metadata),
            )
            .horizontal(),
        )
        .with_default_spacer()
        .with_child(
            Scroll::new(
                Label::dynamic(|d: &TranslationEntry, _| d.dialogue_box.to_string())
                    .with_text_color(TEXT_COLOR)
                    .fix_width(TABLE_HEADER[8].1),
            )
            .horizontal(),
        )
        .with_default_spacer()
        .border(druid::Color::GRAY, 2.0)
}
fn settings_area() -> impl Widget<AppState> {
    let monitors = druid::Screen::get_monitors()
        .into_iter()
        .enumerate()
        .map(|(i, m)| {
            let rect = m.virtual_rect();
            let active_btn = Button::from_label(
                Label::new(if m.is_primary() {
                    "Capture Primary Monitor".into()
                } else {
                    format!("Capture Monitor {}", i)
                })
                .with_text_color(REGULAR_TEXT),
            )
            .fix_width(200.0)
            .on_click(move |ctx, data: &mut AppState, _env| {
                ctx.submit_command(CAPTURE_RECT.with(rect))
            });
            let inactive_btn = Flex::row()
                .with_child(
                    Label::new("Capturing in progress...")
                        .with_text_color(REGULAR_TEXT)
                        .padding(2.0),
                )
                .with_child(Spinner::new().with_color(REGULAR_TEXT));
            Either::new(
                |data: &AppState, env| data.is_capturing_disabled,
                inactive_btn,
                active_btn,
            )
        });

    let mut flex_first = Flex::column().with_flex_spacer(1.0);

    for button in monitors {
        flex_first.add_child(button);
        flex_first.add_flex_spacer(5.0)
    }
    let inactive_btn = Flex::row()
        .with_child(
            Label::new("Capturing in progress...")
                .with_text_color(REGULAR_TEXT)
                .padding(2.0),
        )
        .with_child(Spinner::new().with_color(REGULAR_TEXT));
    let capture_window_btn = Either::new(
        |data: &AppState, env| data.is_capturing_disabled,
        inactive_btn,
        Button::from_label(Label::new("Capture window").with_text_color(REGULAR_TEXT))
            .fix_width(200.0)
            .on_click(move |ctx, data: &mut AppState, _env| {
                ctx.submit_command(CAPTURE_WINDOW);
            }),
    );
    flex_first.add_child(capture_window_btn);
    flex_first.add_flex_spacer(5.0);
    let chapter_textfield = TextBox::new()
        .with_placeholder("Current Chapter")
        .with_text_size(18.0)
        .with_text_color(REGULAR_TEXT)
        .fix_width(200.0)
        .lens(druid::lens!(AppState, translation.current_chapter));

    flex_first.add_child(chapter_textfield);
    let visible_windows = Scroll::new(
        Scroll::new(
            List::new(|| {
                Label::dynamic(|d: &(usize, String), _| d.1.clone())
                    .with_text_color(REGULAR_TEXT)
                    .border(druid::Color::WHITE, 1.0)
            })
            .lens(AppState::visible_windows),
        )
        .vertical(),
    );
    let windows = Flex::column()
        .with_child(Label::new("Open Windows").with_text_color(REGULAR_TEXT))
        .with_default_spacer()
        .with_child(visible_windows.fix_height(120.0))
        .with_default_spacer();
    let window_textfield = TextBox::new()
        .with_placeholder("Window Name")
        .with_text_size(18.0)
        .with_text_color(REGULAR_TEXT)
        .fix_width(200.0)
        .lens(druid::lens!(AppState, translation.current_capture_window));
    let hotkey_settings = Flex::column()
        .with_child(Label::new("Hotkey Captures...").with_text_color(REGULAR_TEXT))
        .with_default_spacer()
        .with_child(
            RadioGroup::new(vec![
                ("Window", HotKey::Window),
                ("Primary", HotKey::Primary),
            ])
            .lens(druid::lens!(AppState, translation.hot_key)),
        )
        .with_default_spacer()
        .with_child(
            Button::from_label(Label::new("Refresh Windows").with_text_color(REGULAR_TEXT))
                .fix_width(200.0)
                .on_click(move |ctx, data: &mut AppState, _env| {
                    data.visible_windows = std::sync::Arc::new(crate::ffi::enumerate_windows());
                }),
        )
        .with_default_spacer()
        .with_child(window_textfield)
        .with_default_spacer();
    let export_textfield = TextBox::new()
        .with_placeholder("Export from line")
        .with_text_size(18.0)
        .with_text_color(REGULAR_TEXT)
        .with_formatter(druid::text::ParseFormatter::<usize>::new())
        .update_data_while_editing(true)
        .fix_width(200.0)
        .lens(druid::lens!(AppState, translation.export_from));
    let export = Flex::column()
        .with_child(
            Button::from_label(Label::new("Export from line:").with_text_color(REGULAR_TEXT))
                .fix_width(200.0)
                .on_click(move |ctx, data: &mut AppState, _env| {
                    data.translation
                        .export_csv(CSV_EXPORT_PATH.as_ref(), data.translation.export_from);
                    data.translation.export_from =
                        data.translation.list.front().map_or(0, |e| e.line + 1)
                }),
        )
        .with_default_spacer()
        .with_child(export_textfield)
        .with_default_spacer();
    Flex::row()
        .with_child(flex_first.padding(10.0))
        .with_child(windows)
        .with_child(hotkey_settings.padding(10.0))
        .with_child(export.padding(10.0))
        .align_horizontal(druid::UnitPoint::CENTER)
        .fix_height(150.0)
}

fn label_widget<T: Data>(widget: impl Widget<T> + 'static, label: &str) -> impl Widget<T> {
    Flex::column()
        .must_fill_main_axis(true)
        .with_flex_child(widget.center().fix_height(50.0), 1.0)
        .with_child(
            Painter::new(|ctx, _: &_, _: &_| {
                let size = ctx.size().to_rect();
                ctx.fill(size, &Color::WHITE)
            })
            .fix_height(1.0),
        )
        .with_child(Label::new(label).center().fix_height(40.0))
        .border(Color::WHITE, 1.0)
}