use winapi::shared::minwindef::{BOOL, LPARAM, TRUE};
use winapi::um::winuser::{EnumWindows, GetWindowTextW, IsWindowVisible};

/// Returns raw bitmap of current screen along width and height
/// //TODO Remove
fn capture_screen_raw() -> (Vec<u8>, i32, i32) {
    let (pixels_raw, width, height) = unsafe { capture_primary_screen_raw() };
    //Magic numbers are size of various BMP headers (File, ...)
    let mut bmp_buffer = Vec::with_capacity((pixels_raw.len() + 14 + 40 + 108) as usize);
    image::codecs::bmp::BmpEncoder::new(&mut bmp_buffer).encode(
        &pixels_raw,
        width as u32,
        height as u32,
        image::ColorType::Rgb8,
    );
    (bmp_buffer, width, height)
}

//TODO Remove
fn capture_screen_virtual_raw(x0: i32, y0: i32, width: i32, height: i32) -> (Vec<u8>, i32, i32) {
    let (pixels_raw, width, height) = unsafe { capture_desktop_raw(x0, y0, width, height) };
    //Magic numbers are size of various BMP headers (File, ...)
    let mut bmp_buffer = Vec::with_capacity((pixels_raw.len() + 14 + 40 + 108) as usize);
    image::codecs::bmp::BmpEncoder::new(&mut bmp_buffer).encode(
        &pixels_raw,
        width as u32,
        height as u32,
        image::ColorType::Rgb8,
    );
    (bmp_buffer, width, height)
}

pub unsafe fn capture_primary_screen_raw() -> (Vec<u8>, i32, i32) {
    use winapi::um::wingdi::*;
    use winapi::um::winuser::*;
    let width = GetSystemMetrics(SM_CXSCREEN);
    let height = GetSystemMetrics(SM_CYSCREEN);
    capture_desktop_raw(0, 0, width, height)
}

pub unsafe fn capture_desktop_raw(
    x0: i32,
    y0: i32,
    width: i32,
    height: i32,
) -> (Vec<u8>, i32, i32) {
    use winapi::um::winuser::*;
    let desktop = GetDesktopWindow();
    capture_window_raw(desktop, x0, y0, width, height)
}

pub unsafe fn capture_window_raw_via_desktop(
    hwnd: winapi::shared::windef::HWND,
) -> (Vec<u8>, i32, i32) {
    use winapi::um::wingdi::*;
    use winapi::um::winuser::*;
    if IsIconic(hwnd) != 0 {
        ShowWindow(hwnd, 9);
    };
    SetForegroundWindow(hwnd);
    // Well,...yeah. ShareX does it too!
    std::thread::sleep(std::time::Duration::from_millis(250));
    let mut info = WINDOWINFO::default();
    info.cbSize = std::mem::size_of::<WINDOWINFO>() as u32;
    GetWindowInfo(hwnd, &mut info); // TODO error
    let mut rect = winapi::shared::windef::RECT::default();
    GetClientRect(hwnd as _, &mut rect);

    let width = rect.right;
    let height = rect.bottom;
    let desktop = GetDesktopWindow();
    capture_window_raw(
        desktop,
        info.rcClient.left,
        info.rcClient.top,
        width,
        height,
    )
}

pub unsafe fn capture_window_raw(
    hwnd: winapi::shared::windef::HWND,
    x0: i32,
    y0: i32,
    width: i32,
    height: i32,
) -> (Vec<u8>, i32, i32) {
    use winapi::um::wingdi::*;
    use winapi::um::winuser::*;
    let hdc_src = GetDC(hwnd);
    let hdc_dest = CreateCompatibleDC(hdc_src);
    let h_bitmap = CreateCompatibleBitmap(hdc_src, width, height);
    let h_old = SelectObject(hdc_dest, h_bitmap as _);
    if BitBlt(
        hdc_dest,
        0,
        0,
        width,
        height,
        hdc_src,
        x0,
        y0,
        SRCCOPY | CAPTUREBLT,
    ) == 0
    {
        log::error!("BitBlt hiccup"); // TODO CHange return to results
    }

    let mut bmi = BITMAPINFO {
        bmiHeader: BITMAPINFOHEADER {
            biSize: std::mem::size_of::<BITMAPINFOHEADER>() as _,
            biWidth: width,
            biHeight: -height,
            biPlanes: 1,
            biBitCount: 8 * 4,
            biCompression: BI_RGB,
            biSizeImage: (width * height * 4) as _,
            biXPelsPerMeter: 0,
            biYPelsPerMeter: 0,
            biClrUsed: 0,
            biClrImportant: 0,
        },
        bmiColors: [RGBQUAD {
            rgbBlue: 0,
            rgbGreen: 0,
            rgbRed: 0,
            rgbReserved: 0,
        }],
    };

    let size_of_pixels = (width * height * 4) as usize;
    let mut pixels: Vec<u8> = Vec::with_capacity(size_of_pixels);

    GetDIBits(
        hdc_dest,
        h_bitmap,
        0,
        height as _,
        pixels.as_mut_ptr() as _,
        &mut bmi,
        DIB_RGB_COLORS,
    );
    pixels.set_len(size_of_pixels);

    // BGRA -> RGBA
    for mut i in (0..pixels.len()).step_by(4) {
        let b = pixels[i];
        let g = pixels[i + 1];
        let r = pixels[i + 2];
        pixels[i] = r;
        pixels[i + 1] = g;
        pixels[i + 2] = b;
    }
    // Get rid of the alpha, its 0xFF anyways.
    // For sure you can do that somehow with the win32 calls
    let pixels: Vec<u8> = pixels
        .into_iter()
        .enumerate()
        .filter(|(i, _)| (i + 1) % 4 != 0)
        .map(|t| t.1)
        .collect();
    SelectObject(hdc_dest, h_old);
    DeleteDC(hdc_dest);
    ReleaseDC(hwnd, hdc_src);

    DeleteObject(h_bitmap as _);
    (pixels, width, height)
}

pub fn enumerate_windows() -> Vec<(usize, String)> {
    let visible_window: Box<Vec<(usize, String)>> = Box::new(Vec::new());
    let ptr = Box::into_raw(visible_window);
    unsafe {
        EnumWindows(Some(enumerate_callback), ptr as _);
        *Box::from_raw(ptr)
    }
}

unsafe extern "system" fn enumerate_callback(
    window: winapi::shared::windef::HWND,
    state: LPARAM,
) -> BOOL {
    if IsWindowVisible(window) != 0 {
        let visible_windows = state as *mut Vec<(usize, String)>;
        let mut title = [0; 1024];
        let length = GetWindowTextW(window, title.as_mut_ptr() as _, 1024);
        if length != 0 {
            if let Ok(title) = String::from_utf16(title[0..(length as usize)].as_ref()) {
                (*visible_windows).push((window as _, title));
            }
        }
    }
    TRUE
}