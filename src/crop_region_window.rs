use std::time::Duration;

use druid::image::{ImageBuffer, Rgb};
use druid::kurbo::RoundedRectRadii;
use druid::piet::{Text, TextLayoutBuilder};
use druid::widget::{BackgroundBrush, Button, Flex, Label, SizedBox};
use druid::{
    AppLauncher, BoxConstraints, Data, Env, Event, EventCtx, HasRawWindowHandle, KbKey, LayoutCtx,
    Lens, LifeCycle, LifeCycleCtx, LocalizedString, MouseButton, PaintCtx, PlatformError,
    RawWindowHandle, RenderContext, Size, UpdateCtx, Widget, WidgetExt, WindowDesc,
};
use winapi::um::winuser::{
    LoadIconA, LoadIconW, SendMessageA, SendMessageW, MAKEINTRESOURCEA, MAKEINTRESOURCEW,
};

use crate::model::AppState;
use crate::primary_window::CAPTURE_RECT;

#[derive(Clone, PartialEq, Data)]
pub(crate) enum CropTarget {
    Line,
    Speaker,
}

impl Default for CropTarget {
    fn default() -> Self {
        CropTarget::Line
    }
}

#[derive(Clone, Data, Lens)]
pub(crate) struct CropRegionState {
    pub is_drawing_rect: bool,
    pub rect_top_left_line: (i32, i32),
    pub rect_bottom_right_line: (i32, i32),
    pub rect_top_left_speaker: (i32, i32),
    pub rect_bottom_right_speaker: (i32, i32),
    pub current_chapter: String,
    pub next_line: usize,
    pub target: CropTarget,
    #[data(ignore)]
    pub sender: crossbeam::channel::Sender<crate::model::TranslationEntry>,
}

pub(crate) fn capture_root(
    pixels: Vec<u8>,
    width: usize,
    height: usize,
    time_now: String,
) -> impl Widget<CropRegionState> {
    RegionCapture::new(pixels, width, height, time_now)
        .controller(crate::primary_window::IconController {})
}

pub(crate) struct RegionCapture {
    pixels: Vec<u8>,
    capture_time: String,
    image_data: druid::ImageBuf,
    paint_image: Option<druid::piet::PietImage>,
    text_layout_size: druid::TextLayout<String>,
    text_layout_line: druid::TextLayout<String>,
    text_layout_speaker: druid::TextLayout<String>,
}

impl RegionCapture {
    pub(crate) fn new(pixels: Vec<u8>, width: usize, height: usize, capture_time: String) -> Self {
        let mut layout = druid::TextLayout::<String>::new();
        layout.set_font(druid::FontDescriptor::default().with_size(24.0));
        layout.set_text_color(druid::Color::GRAY);
        let mut text_layout_line = layout.clone();
        text_layout_line.set_text("Capture Line".to_string());
        let mut text_layout_speaker = layout.clone();
        text_layout_speaker.set_text("Capture Speaker".to_string());
        Self {
            pixels: pixels.clone(),
            capture_time,
            image_data: druid::ImageBuf::from_raw(
                pixels,
                druid::piet::ImageFormat::Rgb,
                width,
                height,
            ),
            paint_image: None,
            text_layout_size: layout,
            text_layout_line,
            text_layout_speaker,
        }
    }
}

impl RegionCapture {
    fn request_ocr(&mut self, data: &mut CropRegionState, speaker: bool) {
        let (width_line, height_line) = (
            data.rect_bottom_right_line.0 - data.rect_top_left_line.0,
            data.rect_bottom_right_line.1 - data.rect_top_left_line.1,
        );
        let (width_speaker, height_speaker) = (
            data.rect_bottom_right_speaker.0 - data.rect_top_left_speaker.0,
            data.rect_bottom_right_speaker.1 - data.rect_top_left_speaker.1,
        );
        let mut img = match image::RgbImage::from_raw(
            self.image_data.width() as u32,
            self.image_data.height() as u32,
            Vec::from(self.image_data.raw_pixels()),
        ) {
            None => {
                log::error!("Could not create ImageBuffer");
                return;
            }
            Some(img) => img,
        };
        image::imageops::invert(&mut img);

        let image_line = self.crop_and_encode_tiff(
            &mut img,
            data.rect_top_left_line.0 as u32,
            data.rect_top_left_line.1 as u32,
            width_line as u32,
            height_line as u32,
        );
        let image_speaker = if speaker {
            Some(self.crop_and_encode_tiff(
                &mut img,
                data.rect_top_left_speaker.0 as u32,
                data.rect_top_left_speaker.1 as u32,
                width_speaker as u32,
                height_speaker as u32,
            ))
        } else {
            None
        };
        data.sender.send(crate::model::TranslationEntry {
            line: data.next_line,
            chapter: data.current_chapter.clone(),
            file_name_line: format!(
                "{}/{:06}-{}.tiff",
                crate::CAPTURES_FOLDER,
                data.next_line,
                &self.capture_time
            ),
            file_name_speaker: image_speaker.as_ref().map(|_| {
                format!(
                    "{}/{:06}-{}-speaker.tiff",
                    crate::CAPTURES_FOLDER,
                    data.next_line,
                    &self.capture_time
                )
            }),
            dialogue_box: crate::model::DialogueRect {
                x: data.rect_top_left_line.0 as usize,
                y: data.rect_top_left_line.1 as usize,
                width: width_line as usize,
                height: height_line as usize,
                anchor_width: self.image_data.width(),
                anchor_height: self.image_data.height(),
            },
            tiff_speaker: image_speaker,
            tiff_line: Some(image_line),
            processing_ocr: true,
            ..Default::default()
        });
    }

    fn crop_and_encode_tiff(
        &mut self,
        img: &mut image::ImageBuffer<image::Rgb<u8>, Vec<u8>>,
        x: u32,
        y: u32,
        width: u32,
        height: u32,
    ) -> Vec<u8> {
        let mut img_cropped_line = image::imageops::crop(img, x, y, width, height);
        let img_cropped_line = img_cropped_line.to_image();

        let mut buffer = std::io::Cursor::new(Vec::with_capacity(
            (img_cropped_line.height() * img_cropped_line.width() + 200) as usize,
        ));
        if let Err(e) = image::codecs::tiff::TiffEncoder::new(&mut buffer).encode(
            img_cropped_line.as_raw(),
            img_cropped_line.width(),
            img_cropped_line.height(),
            image::ColorType::Rgb8,
        ) {
            log::error!("Could not encode image!:\n{:?}", e);
        }
        buffer.into_inner()
    }
}

impl Widget<CropRegionState> for RegionCapture {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut CropRegionState, env: &Env) {
        match event {
            Event::WindowConnected => {
                ctx.request_focus();
                ctx.override_cursor(&druid::Cursor::Crosshair);
                ctx.window().set_title("AzothCapture - Crop")
            }
            Event::WindowCloseRequested => {}
            Event::WindowDisconnected => {
                ctx.clear_cursor();
            }
            Event::WindowSize(_) => {}
            Event::MouseDown(m) if m.button == MouseButton::Left => {
                data.is_drawing_rect = true;
                match data.target {
                    CropTarget::Line => data.rect_top_left_line = (m.pos.x as i32, m.pos.y as i32),
                    CropTarget::Speaker => {
                        data.rect_top_left_speaker = (m.pos.x as i32, m.pos.y as i32)
                    }
                }
                ctx.set_active(true);
                ctx.request_layout();
                ctx.request_paint();
            }
            Event::MouseUp(m) if m.button == MouseButton::Right => {
                if !data.is_drawing_rect {
                    if data.target == CropTarget::Speaker {
                        self.request_ocr(data, false);
                    }
                    ctx.submit_command(
                        crate::primary_window::CAPTURE_CLOSED.to(druid::Target::Global),
                    );
                    ctx.window().close();
                }
            }
            Event::MouseUp(m) if m.button == MouseButton::Left => {
                data.is_drawing_rect = false;
                ctx.set_active(false);
                ctx.request_layout();
                ctx.request_paint();
                let (width, height) = match data.target {
                    CropTarget::Line => (
                        data.rect_bottom_right_line.0 - data.rect_top_left_line.0,
                        data.rect_bottom_right_line.1 - data.rect_top_left_line.1,
                    ),
                    CropTarget::Speaker => (
                        data.rect_bottom_right_speaker.0 - data.rect_top_left_speaker.0,
                        data.rect_bottom_right_speaker.1 - data.rect_top_left_speaker.1,
                    ),
                };
                // TODO transform
                if width > 0 && height > 0 {
                    match data.target {
                        CropTarget::Line => data.target = CropTarget::Speaker,
                        CropTarget::Speaker => {
                            self.request_ocr(data, true);
                            ctx.submit_command(
                                crate::primary_window::CAPTURE_CLOSED.to(druid::Target::Global),
                            );
                            ctx.window().close();
                        }
                    }
                }
            }
            Event::MouseMove(m) => {
                match data.target {
                    CropTarget::Line => {
                        data.rect_bottom_right_line = (m.pos.x as i32, m.pos.y as i32)
                    }
                    CropTarget::Speaker => {
                        data.rect_bottom_right_speaker = (m.pos.x as i32, m.pos.y as i32);
                    }
                }
                if data.is_drawing_rect {
                    ctx.request_layout();
                }
                ctx.request_paint();
            }
            Event::Wheel(_) => {}
            Event::KeyDown(k) if k.key == KbKey::Escape => {
                ctx.submit_command(crate::primary_window::CAPTURE_CLOSED.to(druid::Target::Global));
                ctx.window().close();
            }
            Event::KeyUp(_) => {}
            Event::Paste(_) => {}
            Event::Zoom(_) => {}
            Event::Timer(_) => {}
            Event::AnimFrame(_) => {}
            Event::Command(_) => {}
            Event::Notification(_) => {}
            Event::ImeStateChange => {}
            Event::Internal(_) => {}
            _ => {}
        }
    }

    fn lifecycle(
        &mut self,
        ctx: &mut LifeCycleCtx,
        event: &LifeCycle,
        data: &CropRegionState,
        env: &Env,
    ) {
        match event {
            LifeCycle::WidgetAdded => {}
            LifeCycle::Size(_) => {}
            LifeCycle::DisabledChanged(_) => {}
            LifeCycle::HotChanged(_) => {}
            LifeCycle::BuildFocusChain => {
                ctx.register_for_focus();
                // // NOTE not implemented yet on windows...
                // //ctx.window().bring_to_front_and_focus();
                // // HACK
                // let handle = ctx.window().raw_window_handle();
                // match handle {
                //     RawWindowHandle::Windows(handle) => unsafe {
                //         winapi::um::winuser::SetForegroundWindow(handle.hinstance as _);
                //     },
                //     _ => {}
                // }
            }
            LifeCycle::FocusChanged(_) => {}
            LifeCycle::Internal(_) => {}
        }
    }

    fn update(
        &mut self,
        ctx: &mut UpdateCtx,
        old_data: &CropRegionState,
        data: &CropRegionState,
        env: &Env,
    ) {
        if old_data.is_drawing_rect {
            ctx.request_paint();
        }
        if self.text_layout_size.needs_rebuild_after_update(ctx)
            || self.text_layout_line.needs_rebuild_after_update(ctx)
            || self.text_layout_speaker.needs_rebuild_after_update(ctx)
        {
            ctx.request_layout();
        }
    }

    fn layout(
        &mut self,
        ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        data: &CropRegionState,
        env: &Env,
    ) -> Size {
        let (width, height) = match data.target {
            CropTarget::Line => (
                data.rect_bottom_right_line.0 - data.rect_top_left_line.0,
                data.rect_bottom_right_line.1 - data.rect_top_left_line.1,
            ),
            CropTarget::Speaker => (
                data.rect_bottom_right_speaker.0 - data.rect_top_left_speaker.0,
                data.rect_bottom_right_speaker.1 - data.rect_top_left_speaker.1,
            ),
        };
        self.text_layout_size
            .set_text(format!("[{},{}]", width, height));
        self.text_layout_size.rebuild_if_needed(ctx.text(), env);
        self.text_layout_speaker.rebuild_if_needed(ctx.text(), env);
        self.text_layout_line.rebuild_if_needed(ctx.text(), env);
        bc.constrain(self.image_data.size())
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &CropRegionState, env: &Env) {
        let (top_left, bottom_right, layout) = match data.target {
            CropTarget::Line => (
                data.rect_top_left_line,
                data.rect_bottom_right_line,
                &self.text_layout_line,
            ),
            CropTarget::Speaker => (
                data.rect_top_left_speaker,
                data.rect_bottom_right_speaker,
                &self.text_layout_speaker,
            ),
        };
        let image = {
            let image_data = &self.image_data;
            self.paint_image
                .get_or_insert_with(|| image_data.to_image(ctx.render_ctx))
        };
        ctx.draw_image(
            &image,
            self.image_data.size().to_rect(),
            druid::piet::InterpolationMode::Bilinear,
        );
        ctx.draw_text(
            layout.layout().unwrap(),
            (bottom_right.0 as f64 - 40.0, bottom_right.1 as f64 - 50.0),
        );
        if data.is_drawing_rect {
            let r = druid::Rect::new(
                top_left.0 as f64,
                top_left.1 as f64,
                bottom_right.0 as f64,
                bottom_right.1 as f64,
            );
            ctx.stroke(r, &druid::Color::RED, 2.0);
            ctx.draw_text(
                &self.text_layout_size.layout().unwrap(),
                (bottom_right.0 as f64 + 10.0, bottom_right.1 as f64 + 10.0),
            );
        }
    }
}