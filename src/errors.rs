pub(crate) use anyhow::*;
pub(crate) use log::{debug, error, info, trace, warn};

pub(crate) fn log_error<T>(r: Result<T>) -> Option<T> {
    if let Err(ref e) = r {
        error!("Error level - description");
        e.chain()
            .enumerate()
            .for_each(|(index, error)| error!("└> {} - {:?}", index, error));

        // The backtrace is not always generated. Run with `RUST_BACKTRACE=1`.
        error!("backtrace: {:?}", e.backtrace());
        None
    } else {
        Some(r.unwrap())
    }
}