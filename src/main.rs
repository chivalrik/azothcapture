#![feature(with_options)]
#![recursion_limit = "2048"]
// On Windows platform, don't show a console when opening the app.
#![windows_subsystem = "windows"]
#![feature(windows_process_extensions_raw_arg)]

use crate::primary_window::REGULAR_TEXT;
use anyhow::*;
use log::error;

mod crop_region_window;
mod errors;
mod ffi;
mod main_druid_multiwin;
mod main_druid_subwin;
mod model;
mod ocr;
mod primary_window;
mod tray;
mod view;

// TODO Constants in a mod
pub(crate) const CAPTURES_FOLDER: &str = "captures";

fn main() -> anyhow::Result<()> {
    let log = init_logger();
    if let Err(ref e) = log {
        use std::error::Error;
        use std::io::Write;
        let stderr = &mut ::std::io::stderr();
        // Best effort, if we do not even have a logger, what else remains?
        let _drop = writeln!(stderr, "ERROR: {:?}", e);

        // The backtrace is not always generated. Run with `RUST_BACKTRACE=1`.
        let _drop = writeln!(stderr, "backtrace: {:?}", e.backtrace());

        return log;
    }
    log_panics::init();
    log::info!("AzothCapture - Booting up.");
    let leptess_best = match ocr::set_up_leptess("jpn", "./tessdata/best") {
        Ok(engine) => engine,
        Err(e) => {
            log::error!("Could not set up OCR engine {:?}", e);
            return Err(e);
        }
    };
    let leptess_fast = match ocr::set_up_leptess("jpn", "./tessdata/fast") {
        Ok(engine) => engine,
        Err(e) => {
            log::error!("Could not set up OCR engine {:?}", e);
            return Err(e);
        }
    };
    std::fs::create_dir(CAPTURES_FOLDER);
    let (primary_window, delegate) = primary_window::create();
    let (initial_state, receiver) = model::AppState::new();
    // TODO Disable console tracing on release build
    let launcher = druid::AppLauncher::with_window(primary_window)
        .delegate(delegate)
        .configure_env(|env, _| {
            env.set(
                druid::theme::UI_FONT,
                druid::FontDescriptor::default().with_size(12.0),
            );
            env.set(druid::theme::TEXT_COLOR, REGULAR_TEXT);
            env.set(druid::theme::WIDGET_PADDING_HORIZONTAL, 2.0);
            env.set(druid::theme::WIDGET_PADDING_VERTICAL, 2.0);
        })
        .log_to_console(); // We do not want to impl tracing subscribers so we let druid trace

    let event_sink = launcher.get_external_handle();
    let hwnd = std::sync::Arc::new(std::sync::atomic::AtomicUsize::default());
    let hwnd_clone = std::sync::Arc::clone(&hwnd);
    let tray_thread = std::thread::spawn(move || tray::run(event_sink, hwnd_clone));
    let event_sink = launcher.get_external_handle();
    let ocr_thread =
        std::thread::spawn(move || ocr::run(receiver, event_sink, leptess_best, leptess_fast));
    let launch_result = launcher.launch(initial_state).context("Launch: SNAFU");
    if let Err(ref e) = launch_result {
        error!("Error level - description");
        e.chain()
            .enumerate()
            .for_each(|(index, error)| error!("└> {} - {:?}", index, error));

        // The backtrace is not always generated. Run with `RUST_BACKTRACE=1`.
        error!("backtrace: {:?}", e.backtrace());
    }
    unsafe {
        winapi::um::winuser::PostMessageW(
            hwnd.load(std::sync::atomic::Ordering::SeqCst) as _,
            winapi::um::winuser::WM_QUIT,
            0,
            0,
        )
    };
    tray_thread.join().expect("TrayThread Join SNAFU");
    ocr_thread.join().expect("OcrThread Join SNAFU")?;
    launch_result
}

fn init_logger() -> anyhow::Result<()> {
    let log_config = simplelog::ConfigBuilder::new()
        .set_target_level(simplelog::LevelFilter::Error)
        .set_thread_level(simplelog::LevelFilter::Error)
        .set_time_format_str("%FT%T%.6fZ")
        .build();
    let log_file = {
        #[cfg(debug_assertions)]
        {
            std::fs::File::create("azothcapture_debug.log").unwrap()
        }
        #[cfg(not(debug_assertions))]
        {
            std::fs::File::with_options()
                .create(true)
                .append(true)
                .open("azothsubs.log")
                .unwrap()
        }
    };
    // TODO Does not work -> Got permission denied error
    // let size = log_file.metadata().unwrap().len() / 1_049_000;
    // if size > 10 {
    //     log_file.set_len(0).unwrap();
    // }
    simplelog::CombinedLogger::init(vec![
        #[cfg(debug_assertions)]
        {
            simplelog::TermLogger::new(
                simplelog::LevelFilter::Trace,
                log_config.clone(),
                simplelog::TerminalMode::Mixed,
                simplelog::ColorChoice::Auto,
            )
        },
        simplelog::WriteLogger::new(simplelog::LevelFilter::Trace, log_config, log_file),
    ])
    .context("LogInit: SNAFU")
}