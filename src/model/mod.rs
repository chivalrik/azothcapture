use std::fmt::{Display, Formatter};

use druid::im::Vector;
use druid::{Data, Lens};
use serde::{Deserialize, Serialize};

use crate::primary_window::{SETTINGS_PATH_RON, SETTINGS_PATH_TOML};
use std::rc::Rc;

#[derive(PartialEq, Clone, Data, Serialize, Deserialize, Debug)]
pub(crate) enum HotKey {
    Window,
    Primary,
}

impl Default for HotKey {
    fn default() -> Self {
        HotKey::Window
    }
}

#[derive(Clone, Data, Lens)]
pub(crate) struct AppState {
    pub(crate) translation: TranslationState,
    pub(crate) visible_windows: std::sync::Arc<Vec<(usize, String)>>,
    pub(crate) is_capturing_disabled: bool,
    #[data(ignore)]
    pub(crate) current_line: usize,
    #[data(ignore)]
    pub(crate) sender: crossbeam::channel::Sender<crate::model::TranslationEntry>,
}

impl AppState {
    pub fn new() -> (
        Self,
        crossbeam::channel::Receiver<crate::model::TranslationEntry>,
    ) {
        let (sender, r) = crossbeam::channel::bounded(50);
        let state = TranslationState::load_toml(SETTINGS_PATH_TOML.as_ref());
        (
            Self {
                current_line: state.list.front().map(|e| e.line + 1).unwrap_or(0),
                translation: state,
                visible_windows: std::sync::Arc::new(Vec::new()),
                is_capturing_disabled: false,
                sender,
            },
            r,
        )
    }
}

#[derive(Clone, Data, Lens, Default, Serialize, Deserialize, Debug, PartialEq)]
pub(crate) struct TranslationState {
    pub(crate) current_capture_window: String,
    pub(crate) current_chapter: String,
    pub(crate) export_from: usize,
    pub(crate) hot_key: HotKey,
    pub(crate) list: Vector<TranslationEntry>,
}

impl TranslationState {
    pub(crate) fn load_toml(path: &std::path::Path) -> Self {
        let settings_toml_string = std::fs::read_to_string(path);
        if settings_toml_string.is_err() {
            log::warn!(
                "Settings toml not found, using default.\n\t{:?}",
                settings_toml_string.unwrap_err()
            );
            return Self::default();
        }
        let settings_toml_string = settings_toml_string.unwrap();
        let settings = toml::from_str::<Self>(&settings_toml_string);
        if settings.is_err() {
            log::error!(
                "Could not parse settings toml, using default.\n\t{:?}",
                settings.unwrap_err()
            );
            Self::default()
        } else {
            settings.unwrap()
        }
    }
    pub(crate) fn load_ron(path: &std::path::Path) -> Self {
        let file = match std::fs::File::open(path) {
            Ok(f) => f,
            Err(e) => {
                log::warn!("Settings ron not found, using default.\n\t{:?}", e);
                return Self::default();
            }
        };
        match ron::de::from_reader(file) {
            Ok(s) => s,
            Err(e) => {
                log::error!("Could not parse settings ron, using default.\n\t{:?}", e);
                Self::default()
            }
        }
    }

    pub(crate) fn save_toml(&self, path: &std::path::Path) {
        let settings_string = toml::to_string(self);
        if settings_string.is_err() {
            log::error!(
                "Could not serialize settings to toml.\n\t{:?}",
                settings_string.unwrap_err()
            );
            return;
        }
        if let Err(e) = std::fs::write(path, settings_string.unwrap()) {
            log::error!("Could not save settings to disk!\n\t{:?}", e);
        }
    }

    pub(crate) fn save_ron(&self, path: &std::path::Path) {
        let pretty = ron::ser::PrettyConfig::new()
            .with_depth_limit(2)
            .with_separate_tuple_members(false)
            .with_enumerate_arrays(true)
            .with_decimal_floats(true)
            .with_new_line("\n".into());
        //let settings_string = ron::ser::to_string_pretty(&self, pretty);
        let settings_string = ron::ser::to_string(&self);
        if settings_string.is_err() {
            log::error!(
                "Could not serialize settings to toml.\n\t{:?}",
                settings_string.unwrap_err()
            );
            return;
        }
        if let Err(e) = std::fs::write(path, settings_string.unwrap()) {
            log::error!("Could not save settings to disk!\n\t{:?}", e);
        }
    }

    pub(crate) fn export_csv(&self, path: &std::path::Path, from_line: usize) {
        let mut csv = match csv::WriterBuilder::new()
            .has_headers(false)
            //.quote_style(csv::QuoteStyle::Always)
            .flexible(false)
            .from_path(path)
        {
            Ok(w) => w,
            Err(e) => {
                log::error!("Could not create CSV writer.\n\t{:?}", e);
                return;
            }
        };
        //  Header
        csv.write_record(&[
            "line",
            "chapter",
            "speaker",
            "english",
            "japanese",
            "speaker_japanese",
            "image_line",
            "image_speaker",
            "metadata",
            "ocr_engine",
            "dialogue_box_x",
            "dialogue_box_y",
            "dialogue_box_width",
            "dialogue_box_height",
            "dialogue_box_anchor_width",
            "dialogue_box_anchor_height",
        ]);
        for line in self.list.iter().rev().skip_while(|e| e.line < from_line) {
            match csv.serialize(line) {
                Ok(_) => {}
                Err(e) => log::error!("Could not serialise as entry:\n{:?}\n\t{:?}", line, e),
            }
        }
    }
}

#[derive(Clone, Copy, Data, PartialEq, Serialize, Deserialize, Debug)]
pub(crate) enum OcrEngine {
    TesseractFast,
    TesseractBest,
    EasyOcr,
    OcrSpace,
}

impl Display for OcrEngine {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                OcrEngine::TesseractFast => "TesseractFast",
                OcrEngine::TesseractBest => "TesseractBest",
                OcrEngine::EasyOcr => "EasyOCR",
                OcrEngine::OcrSpace => "OCRSpace",
            }
        )
    }
}

impl Default for OcrEngine {
    fn default() -> Self {
        Self::TesseractFast
    }
}

impl OcrEngine {
    pub(crate) fn next(&self) -> Self {
        match self {
            Self::TesseractFast => Self::TesseractBest,
            Self::TesseractBest => Self::EasyOcr,
            Self::EasyOcr => Self::OcrSpace,
            Self::OcrSpace => Self::TesseractFast,
        }
    }
}

#[derive(Copy, Clone, Data, Lens, Deserialize, Serialize, Debug, PartialEq, Default)]
pub(crate) struct DialogueRect {
    pub(crate) x: usize,
    pub(crate) y: usize,
    pub(crate) width: usize,
    pub(crate) height: usize,
    pub(crate) anchor_width: usize,
    pub(crate) anchor_height: usize,
}

impl Display for DialogueRect {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "({},{}) -> ({}:{}; {},{})",
            self.anchor_width, self.anchor_height, self.x, self.y, self.width, self.height
        )
    }
}

#[derive(Clone, Data, Lens, Deserialize, Serialize, Debug, PartialEq, Default)]
pub(crate) struct TranslationEntry {
    pub(crate) line: usize,
    pub(crate) chapter: String,
    pub(crate) speaker_english: String,
    pub(crate) line_english: String,
    pub(crate) line_japanese: String,
    pub(crate) speaker_japanese: String,
    #[data(ignore)]
    pub(crate) file_name_line: String,
    #[data(ignore)]
    pub(crate) file_name_speaker: Option<String>,
    pub(crate) metadata: String,
    #[data(ignore)]
    pub(crate) ocr_engine: OcrEngine,
    #[data(ignore)]
    pub(crate) dialogue_box: DialogueRect,
    #[serde(skip)]
    pub(crate) processing_ocr: bool,
    #[data(ignore)]
    #[serde(skip)]
    pub(crate) tiff_speaker: Option<Vec<u8>>,
    #[serde(skip)]
    #[data(ignore)]
    pub(crate) tiff_line: Option<Vec<u8>>,
}