use crate::tray::system_tray_ui::SystemTrayUi;
use nwg::{NativeUi, NwgError};

pub struct SystemTray {
    window: nwg::MessageWindow,
    icon: nwg::Icon,
    tray: nwg::TrayNotification,
    tray_menu: nwg::Menu,
    tray_item1: nwg::MenuItem,
    tray_item2: nwg::MenuItem,
    quit_item: nwg::MenuItem,
    sink: druid::ExtEventSink,
}

impl SystemTray {
    fn show_menu(&self) {
        let (x, y) = nwg::GlobalCursor::position();
        self.tray_menu.popup(x, y);
    }

    fn hello1(&self) {
        nwg::modal_info_message(&self.window, "Hello", "Hello World!");
    }

    fn hello2(&self) {
        let flags = nwg::TrayNotificationFlags::USER_ICON | nwg::TrayNotificationFlags::LARGE_ICON;
        self.tray.show(
            "Hello World",
            Some("Welcome to my application"),
            Some(flags),
            Some(&self.icon),
        );
    }

    fn quit(&self) {
        self.sink
            .submit_command(druid::commands::QUIT_APP, (), druid::Target::Auto);
        nwg::stop_thread_dispatch();
    }
}

//
// ALL of this stuff is handled by native-windows-derive
//
mod system_tray_ui {
    use super::*;
    use std::cell::RefCell;
    use std::ops::Deref;
    use std::rc::Rc;

    pub struct SystemTrayUi {
        inner: Rc<SystemTray>,
        default_handler: RefCell<Vec<nwg::EventHandler>>,
        raw_handle: nwg::RawEventHandler,
    }

    impl nwg::NativeUi<SystemTrayUi> for SystemTray {
        fn build_ui(mut data: SystemTray) -> Result<SystemTrayUi, nwg::NwgError> {
            use nwg::Event as E;

            let embedded = nwg::EmbedResource::load(None)?;
            // Resources
            nwg::Icon::builder()
                .source_embed(Some(&embedded))
                //.source_file(Some("azoth.ico"))
                .source_embed_id(1)
                .build(&mut data.icon)?;

            // Controls
            nwg::MessageWindow::builder().build(&mut data.window)?;

            nwg::TrayNotification::builder()
                .parent(&data.window)
                .icon(Some(&data.icon))
                .tip(Some("AzothCapture"))
                .build(&mut data.tray)?;

            nwg::Menu::builder()
                .popup(true)
                .parent(&data.window)
                .build(&mut data.tray_menu)?;

            // nwg::MenuItem::builder()
            //     .text("Hello")
            //     .parent(&data.tray_menu)
            //     .build(&mut data.tray_item1)?;

            // nwg::MenuItem::builder()
            //     .text("Popup")
            //     .parent(&data.tray_menu)
            //     .build(&mut data.tray_item2)?;

            nwg::MenuItem::builder()
                .text("Quit")
                .parent(&data.tray_menu)
                .build(&mut data.quit_item)?;

            let inner = Rc::new(data);
            log::info!("Creating raw event handler");
            let raw_ui = Rc::downgrade(&inner);
            let raw_handle = nwg::bind_raw_event_handler(
                &inner.window.handle,
                0x424242,
                move |_hwnd, msg, _wparam, _lparam| {
                    use winapi::um::winuser::*;
                    if let Some(raw_ui) = raw_ui.upgrade() {
                        match msg {
                            WM_HOTKEY => {
                                log::trace!("Hotkey pressed.");
                                raw_ui.sink.submit_command(
                                    crate::primary_window::HOTKEY_PRESSED,
                                    (),
                                    druid::Target::Auto,
                                );
                            }
                            _ => {}
                        };
                    }
                    None
                },
            )
            .expect("Bad raw handle creation");

            unsafe {
                use winapi::um::winuser::*;
                if RegisterHotKey(
                    inner.window.handle.hwnd().unwrap(),
                    1,
                    (MOD_ALT | MOD_SHIFT | MOD_NOREPEAT) as u32,
                    0x51, //Q
                ) == 0
                {
                    log::error!("Could not register hotkey!");
                } else {
                }
            }

            let ui = SystemTrayUi {
                inner,
                default_handler: Default::default(),
                raw_handle,
            };
            // Events
            let evt_ui = Rc::downgrade(&ui.inner);
            let handle_events = move |evt, _evt_data, handle| {
                if let Some(evt_ui) = evt_ui.upgrade() {
                    match evt {
                        E::OnContextMenu => {
                            if &handle == &evt_ui.tray {
                                SystemTray::show_menu(&evt_ui);
                            }
                        }
                        E::OnMenuItemSelected => {
                            if &handle == &evt_ui.tray_item1 {
                                SystemTray::hello1(&evt_ui);
                            } else if &handle == &evt_ui.tray_item2 {
                                SystemTray::hello2(&evt_ui);
                            } else if &handle == &evt_ui.quit_item {
                                SystemTray::quit(&evt_ui);
                            }
                        }
                        _ => {}
                    }
                }
            };

            ui.default_handler
                .borrow_mut()
                .push(nwg::full_bind_event_handler(
                    &ui.window.handle,
                    handle_events,
                ));

            return Ok(ui);
        }
    }

    impl Drop for SystemTrayUi {
        /// To make sure that everything is freed without issues, the default handler must be unbound.
        fn drop(&mut self) {
            let mut handlers = self.default_handler.borrow_mut();
            for handler in handlers.drain(0..) {
                nwg::unbind_event_handler(&handler);
            }
            nwg::unbind_raw_event_handler(&self.raw_handle);
            unsafe {
                if winapi::um::winuser::UnregisterHotKey(self.window.handle.hwnd().unwrap(), 1) == 0
                {
                    log::error!("Unregistring HotKey failed!");
                }
            }
        }
    }

    impl Deref for SystemTrayUi {
        type Target = SystemTray;

        fn deref(&self) -> &SystemTray {
            &self.inner
        }
    }
}

pub(crate) fn run(sink: druid::ExtEventSink, hwnd: std::sync::Arc<std::sync::atomic::AtomicUsize>) {
    match nwg::init() {
        Ok(_) => {}
        Err(e) => log::error!("Init: {:?}", e),
    }
    let state = SystemTray {
        window: Default::default(),
        icon: Default::default(),
        tray: Default::default(),
        tray_menu: Default::default(),
        tray_item1: Default::default(),
        tray_item2: Default::default(),
        quit_item: Default::default(),
        sink,
    };
    let ui = SystemTray::build_ui(state);
    if let Err(ref e) = ui {
        log::error!("NWG BuildUI: {:?}", e)
    };
    let ui = ui.expect("See logged BuildUI error.");
    hwnd.store(
        ui.window.handle.hwnd().unwrap() as usize,
        std::sync::atomic::Ordering::SeqCst,
    );
    nwg::dispatch_thread_events();
}