use crate::model::{OcrEngine, TranslationEntry};
use anyhow::Context;
use leptess::LepTess;
use log::error;
use serde_json::Value;
use std::io::Error;
use std::os::windows::process::CommandExt;

// NOTE (Chiv) Ideally, the ocr module should only care about the file and which engine to choose and
// where to send with what identifier so the other side knows where it belongs.
// That should be reflected in the channel data having only that info and nothing else.
// Just using the Translation Entry is easier and straightforward though and as this is all _one_
// application, it is fine for now. It is not scalable much more though and depending on needs,
// this might be revisit later.
pub(crate) fn run(
    rx: crossbeam::channel::Receiver<crate::model::TranslationEntry>,
    sink: druid::ExtEventSink,
    mut leptess_best: leptess::LepTess,
    mut leptess_fast: leptess::LepTess,
) -> anyhow::Result<()> {
    log::debug!("OCR Thread Init");
    let ocr_space_api_key =
        std::fs::read_to_string("ocrspace_apikey").unwrap_or("helloworld".into());
    log::debug!("OCRSpace api_key: {}", ocr_space_api_key);
    for mut entry in rx {
        if entry.tiff_line.is_some() {
            log::debug!("Saving capture to {}", &entry.file_name_line);
            if let Err(e) = std::fs::write(&entry.file_name_line, entry.tiff_line.as_ref().unwrap())
            {
                error!("Could not save {}:\n{:?}", &entry.file_name_line, e)
            }
        }
        if entry.tiff_speaker.is_some() {
            if let Err(e) = std::fs::write(
                entry.file_name_speaker.as_ref().unwrap(),
                entry.tiff_speaker.as_ref().unwrap(),
            ) {
                error!(
                    "Could not save {}:\n{:?}",
                    entry.file_name_speaker.as_ref().unwrap(),
                    e
                )
            }
        }
        perform_ocr(
            &mut entry,
            &mut leptess_fast,
            &mut leptess_best,
            &ocr_space_api_key,
        );
        log::debug!(
            "OCR:{}:{}\n{}",
            entry.ocr_engine,
            &entry.speaker_japanese,
            &entry.line_japanese
        );
        sink.submit_command(
            crate::primary_window::ADD_TRANSLATION_ENTRY,
            entry,
            druid::Target::Auto,
        );
    }
    log::debug!("OCR Thread Goodbye");
    Ok(())
}

fn perform_ocr(
    entry: &mut TranslationEntry,
    leptess_fast: &mut leptess::LepTess,
    leptess_best: &mut leptess::LepTess,
    ocr_space_api_key: &String,
) -> anyhow::Result<()> {
    let ((line, speaker), engine) = match entry.ocr_engine {
        OcrEngine::TesseractFast => (ocr_leptess(entry, leptess_fast), OcrEngine::TesseractFast),
        OcrEngine::TesseractBest => (ocr_leptess(entry, leptess_best), OcrEngine::TesseractBest),
        OcrEngine::EasyOcr => match ocr_easy(entry) {
            None => (
                ("Error during EasyOCR, check log".into(), "".into()),
                OcrEngine::EasyOcr,
            ),
            Some(r) => (r, OcrEngine::EasyOcr),
        },
        OcrEngine::OcrSpace => match ocr_space(entry, ocr_space_api_key) {
            None => (
                ("Error during OCRSpace, check log".into(), "".into()),
                OcrEngine::OcrSpace,
            ),
            Some(r) => (r, OcrEngine::OcrSpace),
        },
    };
    entry.line_japanese = line.trim().into();
    entry.speaker_japanese = speaker.trim().into();
    entry.ocr_engine = engine;
    Ok(())
}

fn ocr_space(entry: &mut TranslationEntry, api_key: &str) -> Option<(String, String)> {
    let line = ocr_space_image(&entry.file_name_line, api_key);
    let speaker = if entry.file_name_speaker.is_some() {
        ocr_space_image(entry.file_name_speaker.as_ref().unwrap().as_str(), api_key)
    } else {
        None
    };
    line.map(|l| (l, speaker.unwrap_or_else(|| "".into())))
}

fn ocr_space_image(file_name: &str, api_key: &str) -> Option<String> {
    let client = reqwest::blocking::Client::new();
    // let params = vec![
    //     ("language", "jpn"),
    //     ("filetype", "TIF"),
    //     ("scale", "true"),
    //     ("base64Image",),
    // ];
    let multipart = match reqwest::blocking::multipart::Form::new()
        .text("language", "jpn")
        //.text("filetype", "TIF")
        .text("scale", "true")
        .file("file", file_name)
    {
        Ok(m) => m,
        Err(e) => {
            log::warn!("Error during reading of file:\n{:?}", e);
            return None;
        }
    };
    let res = match client
        .post("https://api.ocr.space/parse/image")
        .header("apikey", api_key)
        //.form(&params)
        .multipart(multipart)
        .send()
    {
        Ok(r) => r,
        Err(e) => {
            log::warn!("Error sending request:\n{:?}", e);
            return None;
        }
    };
    if res.status().is_success() {
        let parsed = res.json::<serde_json::Value>();
        match parsed.map(|v| {
            v["ParsedResults"][0]["ParsedText"]
                .as_str()
                .map(|s| s.to_string())
        }) {
            Ok(v) => v,
            Err(e) => {
                log::warn!("Error parsing JSON:\n{:?}", e);
                None
            }
        }
    } else {
        log::warn!("Error returned by request:\n{:?}", res.text());
        None
    }
}

fn ocr_easy(entry: &mut TranslationEntry) -> Option<(String, String)> {
    let line = ocr_easy_image(&entry.file_name_line);
    let speaker = if entry.file_name_speaker.is_some() {
        ocr_easy_image(entry.file_name_speaker.as_ref().unwrap().as_str())
    } else {
        None
    };
    line.map(|l| (l, speaker.unwrap_or_else(|| "".into())))
}

fn ocr_easy_image(file_name: &str) -> Option<String> {
    const CREATE_NO_WINDOW: u32 = 0x08000000;
    const DETACHED_PROCESS: u32 = 0x00000008;
    const CREATE_UNICODE_ENVIRONMENT: u32 = 0x00000400;
    let cmd = std::process::Command::new("cmd")
        .creation_flags(CREATE_NO_WINDOW)
        .arg("/C")
        .raw_arg(format!("python3 easyocr_wrap.py -f \"{}\"", file_name))
        .spawn()
        .unwrap()
        .wait();

    match cmd {
        Ok(e) => {
            if !e.success() {
                log::warn!("EasyOCR exec failed, exit status:\n{:?}", e,);
                return None;
            }
            match std::fs::read_to_string("azothcapture_easyocr_filepipe") {
                Ok(l) => Some(l),
                Err(e) => {
                    log::warn!("Error during EasyOCR file read:\n{:?}", e);
                    None
                }
            }
        }
        Err(e) => {
            log::warn!("Error during EasyOCR exec:\n{:?}", e);
            return None;
        }
    }
}

fn ocr_leptess(entry: &mut TranslationEntry, engine: &mut leptess::LepTess) -> (String, String) {
    let line = ocr_image_leptess(
        entry.tiff_line.as_ref().map(|v| v.as_slice()),
        &entry.file_name_line,
        engine,
        entry.line,
    );
    let speaker = if entry.tiff_speaker.is_some() || entry.file_name_speaker.is_some() {
        ocr_image_leptess(
            entry.tiff_speaker.as_ref().map(|v| v.as_slice()),
            entry
                .file_name_speaker
                .as_ref()
                .map(|s| s.as_str())
                .unwrap_or(""),
            engine,
            entry.line,
        )
    } else {
        "".into()
    };
    (line, speaker)
}

// TODO (Chiv) Should error bubble up?
fn ocr_image_leptess(
    img: Option<&[u8]>,
    file_name: &str,
    engine: &mut leptess::LepTess,
    line: usize,
) -> String {
    if let Err(e) = if img.is_some() {
        engine.set_image_from_mem(img.unwrap())
    } else {
        engine.set_image(file_name)
    } {
        log::error!("Line{}:\n{:?}", line, e);
        return "OCR error, check log for line".into();
    }
    match engine.get_utf8_text() {
        Ok(ocr) => ocr,
        Err(e) => {
            log::error!("Line{}:\n{:?}", line, e);
            "OCR Error, check log for line".into()
        }
    }
}

pub(crate) fn set_up_leptess(language: &str, tessdata: &str) -> anyhow::Result<leptess::LepTess> {
    let mut lt = leptess::LepTess::new(Some(tessdata), language).context("Lepless Init")?;
    lt.set_variable(leptess::Variable::PreserveInterwordSpaces, "1")
        .context("PreserveInterwordSpaces")?;
    // lt.set_variable(leptess::Variable::TesseditWriteImages, "1")
    //     .unwrap();
    //Treat image as single text line 	PSM_SINGLE_LINE = 7
    //Assume sinlge uniform block of text PSM_SINGLE_BLOCK =6  / PSM_SINGLE_BLOCK_VERT_TEXT = 5
    //Assume sinlge column of text of variable size = 4
    // PSM_RAW_LINE = 13, ///< Treat the image as a single text line, bypassing hacks that are Tesseract-specific.
    // PSM_OSD_ONLY = 0,      ///< Orientation and script detection only.
    lt.set_variable(leptess::Variable::TesseditPagesegMode, "6")
        .context("TesseditPagesegMode")?;
    // TODO That seems maybe erronous
    // lt.set_variable(leptess::Variable::UserDefinedDpi, "300")
    //     .context("UserDefinedDpi")?;
    Ok(lt)
}