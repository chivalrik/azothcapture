# AzothCapture

Capture utility to aid in Something no Kiseki fan-translation.

## Changelog

### 0.6.0

- First alpha test

#### LICENCES

- <div>Saturn.svg Icon made by <a href="https://www.flaticon.com/authors/good-ware" title="Good Ware">Good Ware</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>