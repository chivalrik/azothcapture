use std::env;
use std::path::Path;

fn main() {
    if cfg!(target_os = "windows") {
        let mut res = winres::WindowsResource::new();
        res.set_icon_with_id("azoth.ico", "1")
            .set("ProductName", "AzothCapture");
        res.compile().unwrap();
    }

    copy_to_output("azoth.ico");
    copy_to_output("easyocr_wrap.py");
    copy_to_output("ocrspace_apikey");
    let tessdata_best = Path::new("tessdata").join("best");
    let tessdata_fast = Path::new("tessdata").join("fast");
    copy_to_output(tessdata_best.join("eng.traineddata"));
    copy_to_output(tessdata_best.join("jpn.traineddata"));
    copy_to_output(tessdata_best.join("jpn_vert.traineddata"));
    copy_to_output(tessdata_fast.join("eng.traineddata"));
    copy_to_output(tessdata_fast.join("jpn.traineddata"));
    copy_to_output(tessdata_fast.join("jpn_vert.traineddata"));
}

fn copy_to_output(file: impl AsRef<Path>) {
    copy_to_output_as(file.as_ref(), file.as_ref())
}

fn copy_to_output_as(src: impl AsRef<Path>, dest: impl AsRef<Path>) {
    let from = Path::new(&env::var("CARGO_MANIFEST_DIR").unwrap()).join(src.as_ref());
    let to = binary_output_path().join(dest);
    //let from = std::fs::canonicalize(from).expect("Cannot canonicalize");
    //let to = std::fs::canonicalize(to).unwrap();
    std::fs::create_dir_all(&to.parent().unwrap()).unwrap();
    //println!("cargo:warning=\"copying from {:?} to {:?}\"", from, to);
    let _res = std::fs::copy(&from, &to);
    //println!("cargo:warning=\"{:?}\"", res);
    println!("cargo:rerun-if-changed={}", src.as_ref().display());
}

fn binary_output_path() -> std::path::PathBuf {
    let manifest_dir_string = env::var("CARGO_MANIFEST_DIR").unwrap();
    let build_type = env::var("PROFILE").unwrap();
    let manifest_path = std::path::Path::new(&manifest_dir_string);
    if manifest_path.parent().unwrap().join("Cargo.toml").exists() {
        let lines =
            std::fs::read_to_string(manifest_path.parent().unwrap().join("Cargo.toml")).unwrap();
        if lines.contains("[workspace]") {
            return manifest_path
                .parent()
                .unwrap()
                .join("target")
                .join(build_type);
        }
    }
    manifest_path.join("target").join(build_type)
}