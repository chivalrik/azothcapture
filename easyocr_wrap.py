#! python3
import easyocr
import argparse


def parse_args():
    parser = argparse.ArgumentParser(description="EasyOCR shim")
    parser.add_argument(
        "-f",
        "--file",
        required=True,
        type=str,
        help="input file",
    )
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    reader = easyocr.Reader(["ja"])
    with open('azothcapture_easyocr_filepipe', 'wb') as f:
        for line in reader.readtext(args.file, detail=0):
            f.write(line.encode('utf8'))
            f.write(b'\n')


if __name__ == "__main__":
    main()